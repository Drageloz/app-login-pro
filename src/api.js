const BASE_URL_AUTH = 'http://localhost:3030/api/v1';
const BASE_URL_COURSES = 'http://localhost:3031/api/v1';
let url;
async function callApi(api, endpoint, jwt, options = {}) {
    const headers = new Headers();

    if(api === 'auth'){
      url = BASE_URL_AUTH + '/auth' + endpoint;
    }
    else{
      url = BASE_URL_COURSES + '/courses' + endpoint;
    }
    headers.append('Content-Type', 'application/json');

    if(jwt){
        headers.append('Authorization', 'Bearer '+ jwt);
    }
    options.headers = headers;
    const response = await fetch(url, options);
    const data = await response.json();

    return data;
}

const api = {
  calls: {
    autenthicate(credentials) {
      return callApi('auth','/authenticate', undefined,{
        method: 'POST',
        body: JSON.stringify(credentials),
      });
    },
    validate(jwt) {
      return callApi('auth','/validate', jwt,{
        method: 'GET',
        mode: 'cors',
      });
    },
    read(userId, jwt) {
      return callApi('courses', `/user/${userId}`, jwt, {
        method: 'GET',
        mode: 'cors',
      });
    },
    read_page(pageId, jwt) {
      return callApi('courses', `/course/${pageId}`, jwt, {
        method: 'GET',
        mode: 'cors',
      });
    },
  },
};

export default api;