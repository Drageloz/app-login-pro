import React from 'react'
import DenseTable from './DenseTable'
import '../styles/Information.css'
import Logo from "../assets/media/gcfglobal-color.png"
import api from '../api'
import Cookies from 'universal-cookie'
import CryptoJS from "crypto-js"

class Information extends React.Component {
    cookies = new Cookies();

    state = {
        jsonData: false,
        page: null
    }
    
    
    componentDidMount(){

        let jwt = this.cookies.get('jwt')
        var bytes = CryptoJS.AES.decrypt(localStorage.getItem('user'), 'data_encrypted');
        var user = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        let dataUser = new Promise(async function (resolve, reject){
            let information = await api.calls.read(user, jwt);
            let page = await api.calls.read_page(Object.keys(information.data.courses)[0], jwt);
            resolve({information, page})
        });

        dataUser.then((data) => {
            this.setState({
                data: data,
            })
        })

    }
    
    render(){
        return(
            <React.Fragment>
                <div className="information">
                    <img className="information__logo" src={Logo} alt="logo"></img>
                    <div className="container__data">
                        {
                            this.state.data &&
                            <DenseTable {...this.state.data}></DenseTable>
                        }
                        
                    </div>
                    <img className="information__logo--footer" src={Logo} alt="logo"></img>
                </div>
            </React.Fragment>
        )
    }
}

export default Information;