import React from 'react'
import api from '../api'
import usernameIcon from '../assets/nombre.png'
import passwordKey from '../assets/passwordKey.png'
import '../styles/Login.css'
import Cookies from 'universal-cookie'
import logo from '../assets/media/gcfglobal-color.png'
import studyingImage from '../assets/8.svg'



class Login extends React.Component {
    cookies = new Cookies();
     

    state = {
        credentials: {
            username: '',
            password: '',
        },
        jwt: null,
        error: null,
        status: null,
        user: null,
    }

    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount(){
        if(this.cookies.get('jwt')){
            this.isTokenExpired();
        }
    }

    isTokenExpired = async () =>{
        
        const isExpired = await api.calls.validate(this.cookies.get('jwt'));
        if(isExpired.Authorized){
            
            
            this.props.history.push({
                pathname: '/Information',
              })           
              
              
        }
        else{
            this.cookies.set('jwt', '', {path:'/'})
        }
    }

    getJwt = async () => {
        const jwt = await api.calls.autenthicate(this.state.credentials);
        this.setState({
            jwt: jwt.token,
            error: jwt.error,
            user: jwt.user
        })
        if(this.state.jwt){
            localStorage.setItem('user', this.state.user);
            this.cookies.set('jwt', this.state.jwt);
            this.props.history.push('/information');
        }
        
    }

    //Get and Set Values Of Input
    handleInput = (e) =>{
        this.setState({
            credentials: {
                ...this.state.credentials,
                [e.target.name] : e.target.value
            }
        })
    }


    handleClick(){
        this.getJwt();
    }

    render(){
            return <React.Fragment>
                <div className="container"> 
                    <div className="container__form">
                        <img className="form__logo" src={logo} alt="Logo"></img>
                        <div className="form__textLogin">Please Login With your credentials</div>
                        <form className="form">
                            <div className="form__inputs">
                                {this.state.error === "Forbidden" &&(
                                    <div className="form__textLogin--Incorrect">Credentials are incorrect</div>
                                )}
                                <img className="inputs__icon" src={usernameIcon} alt="Login username"></img>
                                <input onChange={this.handleInput} className="inputs" type="text" name="username" placeholder="Username or Email" value={this.state.credentials.username} required></input>
                            </div>

                            <div className="form__inputs">
                                <img className="inputs__icon" src={passwordKey} alt="Password Key"></img>
                                <input onChange={this.handleInput} className="inputs" type="password" name="password" placeholder="Password" value={this.state.credentials.password} required></input>
                            </div>
                            <button onClick={this.handleClick} className="form__button" type="button">
                                <div className="button__text">Login</div>
                            </button>
                        </form>
                    </div>
                    <div className="container__image">
                        <img  src={studyingImage} alt="studying"></img>
                    </div>

                </div>
            </React.Fragment>
    }
}

export default Login;