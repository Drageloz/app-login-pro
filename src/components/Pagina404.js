import React from 'react'

class Pagina404 extends React.Component{
    render(){
        return <h1>Error 404: Not Found</h1>
    }
}

export default Pagina404;