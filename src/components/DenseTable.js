import React from 'react'
import '../styles/DenseTable.css'
import search from '../assets/search.svg'
import list from '../assets/list.svg'




const listOfCourses = (jsonData, page) =>{

  const handleChange = param => e => {
    let img = document.getElementById("img_"+param);
    let description = document.getElementById("description_"+param);
    
    if(img.className === "listButton__image listButton__image--closed"){
      img.className = "listButton__image listButton__image--open"
      description.className = "description--closed"
    }else{
      img.className = "listButton__image listButton__image--closed"
      description.className = "description--open"
    }


    
    
  };
  
  let coursesList = []
  let keys = Object.keys(jsonData.data.courses);
  //let page = "./course/courseA1";
  for(let i=0; i<Object.keys(jsonData.data.courses).length; i++){
    let status = jsonData.data.courses[keys[i]].status
    coursesList.push(
    <tr key={i}>
      <td>
        <div className="text--padding">
          <div onClick={handleChange(i)} className="container__listButton"><img id={"img_"+i} className="listButton__image listButton__image--open" src={list} alt="list"></img></div>
          <p>{keys[i] + ": " + jsonData.data.courses[keys[i]].name}</p>
        </div>
        <div className={status==='passed' ? 'table__course--passed': 'table__course--failed'}><p>{status.toUpperCase()}</p></div>
        <div className="search__button"><a href={page}><img src={search} alt="search course"></img></a></div>
      </td>
      <td id={"description_"+i} className="description--closed">
        <div >{jsonData.data.courses[keys[i]].description}</div>
      </td>
    </tr>
    )
  }
  return coursesList;
}


const DenseTable = (data) => {
  console.log(data)
  let jsonData = data.information;
  let page = data.page.data;
  
  return(
    <div className="container__table">
      <div className="table__username">
        <div className="text--padding">{"NAME: "} </div>
        <div className="username__name"> {jsonData.data.users.firstname.toUpperCase() + " " + jsonData.data.users.lastname.toUpperCase()} </div>  
      </div>
      <table>
        <tbody>
          <tr className="table__header">
            <td>CURSOS</td>
          </tr>
          {listOfCourses(jsonData, page)}
        </tbody>
      </table>
    </div>
  )


}

export default DenseTable;