import React from "react";
import { Redirect, Route } from "react-router-dom";
import api from '../api'
import Cookies from 'universal-cookie'


class PrivateRoute extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            isLoading: true,
            isLoggedIn: false
        };
    }

    componentDidMount(){
        let cookies = new Cookies();
        let jwt = cookies.get('jwt');

    
        if(jwt){
            let myPromise = new Promise(async function(myResolve, myReject) {
                // "Producing Code" (May take some time)
                    
                let response = await api.calls.validate(jwt);       
                myResolve(response); // when successful
                    
            });
                
                // "Consuming Code" (Must wait for a fulfilled Promise)
            myPromise.then((Authorized) => {
                if(Authorized.Authorized){
                    this.setState(() => ({ isLoading: false, isLoggedIn: true }));
                }else{
                    cookies.set('jwt', '');
                    this.setState(() => ({ isLoading: false, isLoggedIn: false }));
                }
            })
        }
        else{
            this.setState(() => ({ isLoading: false, isLoggedIn: false }));
        }
        

    }

    render() {

        return this.state.isLoading ? null :
            this.state.isLoggedIn ?
            <Route path={this.props.path} component={this.props.component} exact={this.props.exact}/> :
            <Redirect to={{ pathname: '/', state: { from: this.props.location } }} />

    }

}

export default PrivateRoute;