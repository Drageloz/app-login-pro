import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import './App.css';
import Information from './components/Information';
import Login from './components/Login'
import Pagina404 from './components/Pagina404'
import PrivateRoute from './components/PrivateRoute'
import PageA1 from './components/PageA1'

function App() {
  return (
    <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Login} ></Route>
          <PrivateRoute path="/information" component={Information} exact={true}/>
          <PrivateRoute path="/course/courseA1" component={PageA1} exact={true}/>
          <Route path="/404" component={Pagina404} />
          <Redirect from="*" to="/404" />
        </Switch>
    </BrowserRouter>
  );
}

export default App;
